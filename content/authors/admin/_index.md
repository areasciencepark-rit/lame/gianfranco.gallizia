---
# Display name
title: Gianfranco Gallizia

# Full name (for SEO)
first_name: Gianfranco
last_name: Gallizia

# Status emoji
# status:
#   icon:

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Scientific Programmer

# Organizations/Affiliations to show in About widget
organizations:
  - name: AREA Science Park
    url: https://en.areasciencepark.it/rdplatform-2023/lame/

# Short bio (displayed in user profile at end of posts)
bio: Born in 1983, I started using computers at the age of 7 years old and never stopped since.


# Interests to show in About widget
interests:
- Bash/Python scripting.
- Those pesky problems that occasionally stop our datacenter.


# Education to show in About widget
education:
  courses:
  - course: BSc in Computer Science
    institution: Università degli Studi di Trieste
    year: 2011

  - course: HS Diploma in Electronics and Telecommunications
    institution: ITI A. Volta, Trieste
    year: 2004

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: '#contact'
  - icon: github
    icon_pack: fab
    link: https://github.com/ggallizia-AREA
  - icon: linkedin
    icon_pack: fab
    link: https://www.linkedin.com/in/gianfrancogallizia
  # Link to a PDF of your resume/CV.
  # To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.yaml`,
  # and uncomment the lines below.
  #- icon: cv
  #  icon_pack: ai
  #  link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: true
---
Born in 1983, I started using computers at the age of 7 years old and never
stopped since.

My current role is Scientific Developer but most of my day-to-day revolves
around fixing things rather than building new things.

When I'm not looking and/or cursing at a computer screen I enjoy the following
things:

- Walking
- Talking about nerdy stuff
- Videogames
